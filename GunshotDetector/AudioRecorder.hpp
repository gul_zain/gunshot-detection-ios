//
//  AudioRecorder.h
//  GunshotDetector
//
//  Created by KCI on 1/6/19.
//  Copyright © 2019 BlueShark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>
#import "GunshotDetect.hpp"
#import "Constant.h"

#define NUM_BUFFERS 1
#define AUDIO_DATA_TYPE_FORMAT float

typedef struct
{
    AudioStreamBasicDescription dataFormat;
    AudioQueueRef               queue;
    AudioQueueBufferRef         buffers[NUM_BUFFERS];
    AudioFileID                 audioFile;
    SInt64                      currentPacket;
    bool                        recording;
}RecordState;

void AudioInputCallback(void * inUserData,  // Custom audio metadata
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs);



@interface AudioRecorder : NSObject {
    RecordState recordState;
    GunshotDetect* gunshotDetector;
}

- (id)init;
- (void)setupAudioFormat:(AudioStreamBasicDescription*)format;
- (void)startRecording;
- (void)stopRecording;
- (void)feedSamplesToEngine:(UInt32)audioDataBytesCapacity audioData:(void *)audioData;

@end


