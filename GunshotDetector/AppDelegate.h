//
//  AppDelegate.h
//  GunshotDetector
//
//  Created by admin on 1/6/19.
//  Copyright © 2019 BlueShark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

