//
//  ViewController.m
//  GunshotDetector
//
//  Created by admin on 1/6/19.
//  Copyright © 2019 BlueShark. All rights reserved.
//

#import "ViewController.h"

#import "AudioRecorder.hpp"

#import <AudioToolbox/ExtendedAudioFile.h>


@interface ViewController ()

@end

@implementation ViewController{
    AudioRecorder *m_AudioRecorder;
    Boolean isRecording;
    //table view
    NSArray* wav_files;
    GunshotDetect* mDetector;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    m_AudioRecorder = [[AudioRecorder alloc] init];
    isRecording = NO;
    wav_files = [[NSArray alloc] initWithObjects:@"gun1.wav", @"gun2.wav", @"gun3.wav", @"gun4.wav",@"gun5.wav", @"gun6.wav", @"gun7.wav", @"gun8.wav", @"gun9.wav", @"gun10.wav", @"nongun1.wav", @"nongun2.wav", @"nongun3.wav", @"nongun4.wav", @"nongun5.wav", @"nongun6.wav", @"nongun7.wav", @"nongun8.wav", @"nongun9.wav", @"nongun10.wav", nil];
    mDetector = [[GunshotDetect alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ShowResult:) name:NOTIFICATION_GUNSHOT_DETECTED object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:NO];
    [m_AudioRecorder stopRecording];
    m_AudioRecorder = nil;
}

- (IBAction)onBtnRecordClicked:(id)sender {
    if (isRecording)
    {
        [m_AudioRecorder stopRecording];
        isRecording = NO;
        [self.m_record setTitle:@"Record" forState:UIControlStateNormal];
    }
    else{
        [m_AudioRecorder startRecording];
        isRecording = YES;
        [self.m_record setTitle:@"Stop" forState:UIControlStateNormal];
        
        
    }
}

- (void) ShowAlert:(NSString *)Message {
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIView *firstSubview = alert.view.subviews.firstObject;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    for (UIView *subSubView in alertContentView.subviews) {
        subSubView.backgroundColor = [UIColor colorWithRed:141/255.0f green:0/255.0f blue:254/255.0f alpha:1.0f];
    }
    NSMutableAttributedString *AS = [[NSMutableAttributedString alloc] initWithString:Message];
    [AS addAttribute: NSForegroundColorAttributeName value: [UIColor whiteColor] range: NSMakeRange(0,AS.length)];
    [alert setValue:AS forKey:@"attributedTitle"];
    [self presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^{
        }];
    });
}

- (NSArray*) ReadPCMData:(NSString*)path {
    //NSString *  name = @"Filename";  //YOUR FILE NAME
    //NSString * source = [[NSBundle mainBundle] pathForResource:name ofType:@"m4a"]; // SPECIFY YOUR FILE FORMAT
    
    //const char *cString = [source cStringUsingEncoding:NSASCIIStringEncoding];
    const char * cString = [path cStringUsingEncoding:NSASCIIStringEncoding];
    
    CFStringRef str = CFStringCreateWithCString(
                                                NULL,
                                                cString,
                                                kCFStringEncodingMacRoman
                                                );
    CFURLRef inputFileURL = CFURLCreateWithFileSystemPath(
                                                          kCFAllocatorDefault,
                                                          str,
                                                          kCFURLPOSIXPathStyle,
                                                          false
                                                          );
    
    ExtAudioFileRef fileRef;
    ExtAudioFileOpenURL(inputFileURL, &fileRef);
    
    
    AudioStreamBasicDescription audioFormat;
    audioFormat.mSampleRate = 16000;   // GIVE YOUR SAMPLING RATE
    audioFormat.mFormatID = kAudioFormatLinearPCM;
    audioFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat;
    audioFormat.mBitsPerChannel = sizeof(Float32) * 8;
    audioFormat.mChannelsPerFrame = 1; // Mono
    audioFormat.mBytesPerFrame = audioFormat.mChannelsPerFrame * sizeof(Float32);  // == sizeof(Float32)
    audioFormat.mFramesPerPacket = 1;
    audioFormat.mBytesPerPacket = audioFormat.mFramesPerPacket * audioFormat.mBytesPerFrame; // = sizeof(Float32)
    
    // 3) Apply audio format to the Extended Audio File
    ExtAudioFileSetProperty(
                            fileRef,
                            kExtAudioFileProperty_ClientDataFormat,
                            sizeof (AudioStreamBasicDescription), //= audioFormat
                            &audioFormat);
    
    int numSamples = 1024; //How many samples to read in at a time
    UInt32 sizePerPacket = audioFormat.mBytesPerPacket; // = sizeof(Float32) = 32bytes
    UInt32 packetsPerBuffer = numSamples;
    UInt32 outputBufferSize = packetsPerBuffer * sizePerPacket;
    
    // So the lvalue of outputBuffer is the memory location where we have reserved space
    UInt8 *outputBuffer = (UInt8 *)malloc(sizeof(UInt8 *) * outputBufferSize);
    
    
    
    AudioBufferList convertedData ;//= malloc(sizeof(convertedData));
    
    convertedData.mNumberBuffers = 1;    // Set this to 1 for mono
    convertedData.mBuffers[0].mNumberChannels = audioFormat.mChannelsPerFrame;  //also = 1
    convertedData.mBuffers[0].mDataByteSize = outputBufferSize;
    convertedData.mBuffers[0].mData = outputBuffer; //
    
    UInt32 frameCount = numSamples;
    float *samplesAsCArray;
    int total_read =0;
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    while (frameCount > 0) {
        ExtAudioFileRead(
                         fileRef,
                         &frameCount,
                         &convertedData
                         );
        if (frameCount > 0)  {
            AudioBuffer audioBuffer = convertedData.mBuffers[0];
            samplesAsCArray = (float *)audioBuffer.mData; // CAST YOUR mData INTO FLOAT
            //NSArray* samplesAsNSArray = audioBuffer.mdata
            //[array addObjectsFromArray:samplesAsCArray];
        
            for (int i = 0; i < frameCount; i++)
            {
                [array addObject:[NSNumber numberWithFloat:samplesAsCArray[i]]];
            }
            total_read += frameCount;
        }
    }
    return array;  //[array copy];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [wav_files count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:16.0];
    }
    NSString *file = [wav_files objectAtIndex:indexPath.row];
    cell.textLabel.text = file;    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selected = [wav_files objectAtIndex:indexPath.row];
    NSString *file = [[NSBundle mainBundle] pathForResource:selected ofType:nil];
    NSArray* wav_data = [self ReadPCMData:file];
    int cnt = (int)[wav_data count];
    float* floatArray = (float*)malloc(sizeof(float) * cnt);
    for (int i = 0; i < cnt; i++)
    {
        floatArray[i] = [[wav_data objectAtIndex:i] floatValue];
    }
    NSInteger res = [mDetector PredictWithSegment:floatArray :cnt];
    floatArray = nil;
    wav_data = nil;
    if (res == 0)
        //[_mResultLabel setText:@"GunshotDetected."];
        [self ShowAlert:@"Gunshot Detected."];
    //else [_mResultLabel setText:@"No Gunshot."];
    else [self ShowAlert:@"No Gunshot Detected."];
}

#pragma mark -- NOTIFICATION
- (void)ShowResult:(NSNotification*)notification
{
//    NSInteger res = (NSInteger)notification.object;
//    if (res == 0)
//        [_mResultLabel setText:@"GunshotDetected."];
//    else [_mResultLabel setText:@"No Gunshot."];
    //[_mResultLabel setText:@"GunshotDetected."];
    [self ShowAlert:@"Gunshot Detected."];
}
@end
