//
//  ViewController.h
//  GunshotDetector
//
//  Created by admin on 1/6/19.
//  Copyright © 2019 BlueShark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioRecorder.hpp"
//#import "GunshotDetect.h"

@interface ViewController <AudioRecorderDelegate, UITableViewDataSource, UITableViewDelegate>: UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *m_record;
//@property (retain, nonatomic) GunshotDetect* mDetector;
@property (weak, nonatomic) IBOutlet UILabel *mResultLabel;


//- (void) ShowAlert:(NSString *)Message;

- (NSArray*) ReadPCMData:(NSString*) path;
@end

