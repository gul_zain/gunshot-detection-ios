#pragma once
#include "DSP/mfcc.h"
#include "GMMSet.h"

#ifdef __cplusplus
extern "C"{
#endif

class ModelInterface
{
public:
    ModelInterface();
	ModelInterface(int nlabels);
	~ModelInterface();
	void Enroll(int label, const char* wav_path);
	void Train();
	int Predict(float* wav_data, int size);
	int PredictWithFile(const char* wav_path);

	int FindGunshot(const char* wav_file);
    int FindGunshotWithRaw(float* wav_data, int wav_len);

	void Dump(const char* filename);
	void Load(vector<const char*>& model_paths);
	vector<vector<vector<double>>> features;
	int feat_len;
	int nLabels;
	GMMSet mGmmSets;
	MFCC *featExtractor;
};

#ifdef __cplusplus
}
#endif
