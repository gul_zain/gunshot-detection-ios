#include <fstream>
#include "GMMSet.h"


GMMSet::GMMSet()
{
	mMixtures = 64;
	mRejectThreshold = 10;
	params.nr_mixture = mMixtures;
	covariance_type = COVTYPE_DIAGONAL;
	params.min_covar = 1e-3;
	params.threshold = 0.01;
	params.nr_iteration = 200;
	params.init_with_kmeans = 0;
	params.concurrency = 2; // cpu_count();
	params.verbosity = 0;
}

GMMSet::GMMSet(int mixtures, double reject_threshold) {
	this->mMixtures = mixtures;
	this->mRejectThreshold = reject_threshold;
}

GMMSet::~GMMSet()
{
	mGmms.clear();
	mLabels.clear();
}

void GMMSet::Fit(vector<vector<double>>& X, int label) {
	mLabels.push_back(label);
	std::cout << "-->Label-" << label << " Training..." << endl;
	GaussianMixtureModel* gmm = new GaussianMixtureModel(mMixtures, COVTYPE_DIAGONAL);
	params.nr_instance = (int)X.size();
	params.nr_dim = (int)X[0].size();
	gmm->TrainModel(X, &params);
	mGmms.push_back(gmm);	
}

double GMMSet::ScoreInstance_one(int idx, vector<double>& x)
{
	return mGmms[idx]->ScoreInstance(x);
}

double* GMMSet::ScoreAll_one(vector<double>& x)
{
	int i;
	int cnt = (int)mGmms.size();
	double *res = new double[cnt];
	for (i = 0; i < cnt; i++)
		res[i] = ScoreInstance_one(i, x);
	return res;
}

double GMMSet::ScoreInstance(int idx, vector<vector<double>>& X)
{
	return mGmms[idx]->ScoreAll(X, params.concurrency);
}

double* GMMSet::ScoreAll(vector<vector<double>>& X)
{
	int i;
	int cnt = (int)mGmms.size();
	double *res = new double[cnt];
	for (i = 0; i < cnt; i++)
		res[i] = ScoreInstance(i, X);
	return res;
}


int GMMSet::Predict_one(vector<double>& x)
{
	double* scores = ScoreAll_one(x);
	int res = 0;
	int i;
	int gmm_cnt = (int)mGmms.size();
	double maxScore = scores[0];
	for (i = 1; i < gmm_cnt; i++)
	{
		if (maxScore < scores[i]) {
			maxScore = scores[i];
			res = i;
		}
	}
	return res;
}

int GMMSet::Predict(vector<vector<double>>& X)
{
    //if (X.size() < 4) return 1;
	double* scores = ScoreAll(X);
	int res = 0;
	int i;
	int gmm_cnt = (int)mGmms.size();
	double maxScore = scores[0];
//    for (i = 1; i < gmm_cnt; i++)
//    {
//        //NSLog(@"%lf, %lf\n", scores[i]);
//        if (maxScore < scores[i]) {
//            maxScore = scores[i];
//            res = i;
//        }
//    }
    double gunshot_score = scores[0];
    double non_gunshot_score = scores[1];
    if (non_gunshot_score + 23 < gunshot_score)
    {
        res = 0;
    }
    else {
        res = 1;
    }
	return res;
}

void GMMSet::Load(vector<const char*> model_paths)
{
	int i;
	int cnt = (int)model_paths.size();
	mGmms.reserve(cnt);
	for (i = 0; i < cnt; i++)
	{
		GaussianMixtureModel *gmm = new GaussianMixtureModel(model_paths[i]);
		mGmms.push_back(gmm);
	}
}

void GMMSet::Dump(const char * prefix_name)
{
	int cnt = (int)mGmms.size();
	int i;
	for (i = 0; i < cnt; i++)
	{
		char gmm_name[256];
		sprintf(gmm_name, "model/%s_%d.model", prefix_name, i);
		std::cout << "-->" << gmm_name << " saving..." << endl;
		ofstream fout(gmm_name);
		mGmms[i]->Dump(fout);
	}
}
