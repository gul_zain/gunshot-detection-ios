#pragma once
#include "GaussianMixtureModel.h"

using namespace std;


class GMMSet
{
public:
	GMMSet();
	GMMSet(int gmm_order, double reject_threshold);
	~GMMSet();

	void Fit(vector<vector<double>>& X, int label);
	double ScoreInstance_one(int idx, vector<double>&);
	double* ScoreAll_one(vector<double>&);
	double ScoreInstance(int idx, vector<vector<double>>& X);
	double* ScoreAll(vector<vector<double>>& X);

	int Predict_one(vector<double>&);
	int Predict(vector<vector<double>>& X);

	void Load(vector<const char*> model_paths);

	void Dump(const char* prefix_name);


	vector<GaussianMixtureModel*> mGmms;
	int mMixtures;
	double mRejectThreshold;
	vector<int> mLabels;
	int covariance_type;
	//Parameter *params;
	GMMParameter params;
};

