// XAudio.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "DSP/CWave.h"
#include "ModelInterface.h"
#include <vector>

using namespace std;

#define BUFSIZE 512


void get_paths(const char* path_txt, vector<const char*>& paths)
{
	FILE *pf;
	pf = fopen(path_txt, "r");
	if (pf)
	{
		char temp[256];
		while (fgets(temp, 255, pf))
		{
			int len = (int)strlen(temp);
			temp[len - 1] = '\0';
			len -= 1;
			if (len == 0) {
				std::cout << "invalid len --" << endl;
				continue;
			}
			char *temp1 = new char[len+1];
			strcpy(temp1, temp);
			paths.push_back(temp1);
		}
		fclose(pf);
	}
}

void TrainModel(vector<const char*>& data_path, const char* model_prefix)
{
	int i,j, label_count, files_count;
	label_count = data_path.size();
	ModelInterface* modelMgr = new ModelInterface(label_count);
	std::cout << "Enrolling..." << endl;
	for (i = 0; i < label_count; i++)
	{
		vector<const char*> paths;
		get_paths(data_path[i], paths);
		files_count = paths.size();
		for (j = 0; j < files_count; j++)
		{
			std::cout << "-->" << paths[j] << endl;
			modelMgr->Enroll(i, paths[j]);
		}
	}
	std::cout << "Training started..." << endl;
	modelMgr->Train();
	std::cout << "Training finished..." << endl;
	std::cout << "Model Saving..." << endl;
	modelMgr->Dump(model_prefix);
}


void TrainTest()
{
	vector<const char*> train_path;

	train_path.push_back("train/gun_shot.txt");
	train_path.push_back("train/non-gunshot.txt");
	TrainModel(train_path, "gunshot");
}

void PredictTest(vector<const char*>& test_path, int nType)
{
	ModelInterface* modelMgr = new ModelInterface(2);
	vector<const char*> modelpaths;
	modelpaths.push_back("model/gunshot_0.model");
	modelpaths.push_back("model/gunshot_1.model");
	modelMgr->Load(modelpaths);

	int i,j, files_count, res;
	int gun_match, non_gun_match, gun_mismatch, non_gun_mismatch;
	gun_match = 0;
	non_gun_match = 0;
	gun_mismatch = 0;
	non_gun_mismatch = 0;
	const char* label[3] = { "Gunshot", "Non Gunshot" };
	int label_count = (int)test_path.size();
	for (i = 0; i < label_count; i++)
	{
		vector<const char*> paths;
		get_paths(test_path[i], paths);
		files_count = (int)paths.size();
		for (j = 0; j < files_count; j++)
		{
			if (!nType)
				res = modelMgr->PredictWithFile(paths[j]);
			else res = modelMgr->FindGunshot(paths[j]);

			if (i == 0 && res == i) gun_match++;
			else if (i == 0 && res != i) {
				gun_mismatch++;
				printf("mismatched -- %s : %s --> %s\n", paths[j], label[i], label[res]);
			}
			else if (i == 1 && res == i) non_gun_match++;
			else if (i == 1 && res != i) {
				non_gun_mismatch++;
				printf("mismatched -- %s : %s --> %s\n", paths[j], label[i], label[res]);
			}
		}
	}
	printf("\n---------------------------------\n");
	std::cout << "Result:" << endl;
	printf("Gunshot      total    : %d\n", gun_match + gun_mismatch);
	printf("             match    : %d\n", gun_match);
	printf("             mismatch : %d\n", gun_mismatch);
	printf("Perception            : %.5lf\n", (float)gun_match / (gun_match + gun_mismatch) * 100.0);

	printf("NonGunShot   total    : %d\n", non_gun_match + non_gun_mismatch);
	printf("             match    : %d\n", non_gun_match);
	printf("             mismatch : %d\n", gun_mismatch);
	printf("Perception            : %.5lf\n", (float)non_gun_match / (non_gun_match + non_gun_mismatch) * 100.0);

	printf("---------------------------------\n");
	printf("Total  : %.5lf\n", (float)(gun_match + non_gun_match) / (gun_match + gun_mismatch + non_gun_match + non_gun_mismatch) * 100.0);

	delete modelMgr;
}


int main__()
{
    std::cout << "Hello Gentlemen!\n";
	/*DWORD start_tick = GetTickCount();
	TrainTest();
	DWORD end_tick = GetTickCount();
	std::cout << "train time: "<<(end_tick-start_tick)/1000.0 << endl;*/

	vector<const char*> test_path;
	test_path.push_back("train/gun_shot.txt");
	test_path.push_back("train/non-gunshot.txt");

	printf("======================================\n");
	printf("Segment Method Result:\n");
	PredictTest(test_path, 1);

	printf("======================================\n");
	printf("General Method Result:\n");
	PredictTest(test_path, 0);
	//PredictTest("test/nongunshot/");
    return 0;
}





