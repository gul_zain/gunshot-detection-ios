#include "ModelInterface.h"

#include "DSP/CWave.h"

using namespace std;

ModelInterface::ModelInterface()
{
    int sample_rate = 16000;
    featExtractor = new MFCC(16000, 1024, (int)(sample_rate * 0.05), (int)(sample_rate * 0.025), 40, 20);
    this->nLabels = 2;
    features.resize(2);
}

ModelInterface::ModelInterface(int nLabels)
{
	int sample_rate = 16000;
	featExtractor = new MFCC(16000, 1024, (int)(sample_rate * 0.05), (int)(sample_rate * 0.025), 40, 20);
	this->nLabels = nLabels;
	features.resize(nLabels);
}


ModelInterface::~ModelInterface()
{
	int i;
	for (i = 0; i < nLabels; i++)
		features[i].clear();
	features.clear();
}

void cvt_vv2pp(vector<double*> src, double**dst, int& instances)
{
	int i;
	int cnt = (int)src.size();
	instances = cnt;
	for (i = 0; i < cnt; i++)
	{
		dst[i] = src[i];
	}
}

void ModelInterface::Enroll(int label, const char * wav_path)
{
	AudioData *wav_data;
	wav_data = wav_read_float(wav_path);
	if (!wav_data) return;

	std::cout<<wav_path << " is processing..." << endl;
	featExtractor->CalcFeatures(wav_data->data, wav_data->size, FEAT_MFCC, 5, features[label]);
	std::cout << wav_path << " feature calculated..." << endl;
	delete wav_data;
	wav_data = NULL;
}



void ModelInterface::Train()
{
	int i;
	int m = (int)features.size();
	
	for (i = 0; i < m; i++)
	{
		mGmmSets.Fit(features[i], i);
	}
}

int ModelInterface::Predict(float * wav_data, int size)
{
	vector<vector<double>> feat;
	featExtractor->CalcFeatures(wav_data, size, FEAT_MFCC, 5, feat);
	if (feat.size() == 0) return 1;
	return mGmmSets.Predict(feat);
}

int ModelInterface::FindGunshot(const char* wav_file)
{
	AudioData * wav_data = wav_read_float(wav_file);
	int wav_len = wav_data->size;
	int samplerate = wav_data->samplerate;
	int win_step = (int)(0.5 * samplerate);
	int pos = 0;

	if (wav_len < win_step * 4) {
		int res = Predict(wav_data->data, wav_data->size);
		delete wav_data;
		return res;
	}

	float* buf = new float[win_step];
	int counter = 0;
	
	while (wav_len > (pos + win_step))
	{
		if (wav_len - pos < (int)(0.05 * samplerate))
			break;
		memcpy(buf, wav_data->data + pos, sizeof(float) * win_step);
		int res = Predict(buf, win_step);
		if (res == 0) counter++;
		if (counter >= 4)
			break;
		pos += win_step;
	}
	delete[] buf;
	delete[] wav_data->data;
	wav_data = NULL;
	if (counter >= 4)
		return 0;
	else return 1;
}

int ModelInterface::FindGunshotWithRaw(float* wav_data, int wav_len)
{
    int samplerate = 16000;
    int win_size = (int)(0.15 * samplerate);
    int win_step = 320;
    int pos = 0;
    
    if (wav_len < win_size * 4) {
        int res = Predict(wav_data, wav_len);
        delete wav_data;
        return res;
    }
    
    float* buf = new float[win_size];
    int counter = 0;
    
    while (wav_len > (pos + win_size))
    {
        memcpy(buf, wav_data + pos, sizeof(float) * win_size);
        int res = Predict(buf, win_size);
        if (res == 0) counter++;
        if (counter >= 2)
            break;
        pos += win_step;
    }
    delete[] buf;
    //delete[] wav_data;
    //wav_data = NULL;
    if (counter >= 2)
        return 0;
    else return 1;
}

int ModelInterface::PredictWithFile(const char * wav_path)
{
	AudioData * wav_data = wav_read_float(wav_path);
	return Predict(wav_data->data, wav_data->size);
}

void ModelInterface::Dump(const char * prefix)
{
	//mGmmSets.BeforePickle();
	mGmmSets.Dump(prefix);
	//mGmmSets.AfterPickle();
}

void ModelInterface::Load(vector<const char*>& model_paths)
{
	mGmmSets.Load(model_paths);
}

