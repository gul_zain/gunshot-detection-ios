#include "CWave.h"
//#include "sndfile.h"
#include <stdlib.h>
#include <memory.h>

#define BUFSIZE 512


short int* wav_read_short(const char* path)
{
    /**
	SF_INFO sfInfo;
	SNDFILE *pfile;
	int frameCount, channels, totalReadCount;
	short int* buf1, *buf2;
	short int* buffer;
	
	if (!(pfile = sf_open(path, SFM_READ, &sfInfo)))
	{
		printf("Error : could not open \"%s\.\n", path);
		puts(sf_strerror(NULL));
		return NULL;
	}
	frameCount = (int)sfInfo.frames;
	channels = (int)sfInfo.channels;
	buffer = new short int[frameCount * channels];
	buf1 = new short int[BUFSIZE * channels];
	buf2 = new short int[BUFSIZE];
	totalReadCount = 0;
	while (totalReadCount < frameCount)
	{
		int reads = (int)sf_readf_short(pfile, buf1, BUFSIZE);
		wav2mono(buf1, channels, BUFSIZE, buf2);
		memcpy(buffer + totalReadCount, buf2, BUFSIZE);
		totalReadCount += reads;
	}
	delete buf1;
	delete buf2;
	sf_close(pfile);
	return buffer;
     */
    return nullptr;
}


AudioData* wav_read_float(const char* path)
{
    /**
	SF_INFO sfInfo;
	SNDFILE *pfile;
	int frameCount, samplerate, channels, totalReadCount;
	float* buf1, *buf2;
	float* buffer;

	if (!(pfile = sf_open(path, SFM_READ, &sfInfo)))
	{
		printf("Error : could not open \"%s\.\n", path);
		puts(sf_strerror(NULL));
		return NULL;
	}
	frameCount = (int)sfInfo.frames;
	channels = (int)sfInfo.channels;
	samplerate = sfInfo.samplerate;
	buffer = new float[frameCount];
	buf1 = new float[BUFSIZE * channels];
	buf2 = new float[BUFSIZE];
	totalReadCount = 0;
	while (totalReadCount < frameCount)
	{
		int reads = (int)sf_readf_float(pfile, buf1, BUFSIZE);
		wav2mono(buf1, channels, reads, buf2);
		memcpy(buffer + totalReadCount, buf2, reads * 4);
		totalReadCount += reads;
	}
	delete buf1;
	delete buf2;
	sf_close(pfile);
	return new AudioData(buffer, totalReadCount, samplerate);
    */
    return nullptr;
}


void wav2mono_short(short int* buf, int channelCount, int buf_size, short int *dst)
{
    if (channelCount == 1)
    {
        memcpy(dst, buf, sizeof(short int) * buf_size);
    }
    else
    {
        int i, j;
        int temp;
        for (i = 0; i < buf_size; i++)
        {
            temp = 0;
            for (j = 0; j < channelCount; j++)
                temp += (int)buf[i * channelCount + j];
            dst[i] = (short int)(temp / channelCount);
        }
    }
}

void wav2mono(float* buf, int channelCount, int buf_size, float *dst)
{
	if (channelCount == 1)
	{
		memcpy(dst, buf, sizeof(float) * buf_size);
	}
	else
	{
		int i, j;
		float temp;
		for (i = 0; i < buf_size; i++)
		{
			temp = 0.0;
			for (j = 0; j < channelCount; j++)
				temp += buf[i * channelCount + j];
			dst[i] = temp / (float)channelCount;
		}
	}
}


