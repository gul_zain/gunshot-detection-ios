#pragma once
#include "common.h"
using namespace std;

void FFT(unsigned long fftlen, complex<double>* fftbuffer);

void zero_fft(double *data, int size, int fftLen, complex<double>* fftbuffer);
