#pragma once
#include <math.h>
#include <vector>
#include <complex> 
#include <bitset> 

const double PI = 3.1415926536;


void dump_variable(double* v, int size, const char * filename);
void dump_variable(double** v, int m, int n, const char* filename);
