//
//  main.m
//  GunshotDetector
//
//  Created by admin on 1/6/19.
//  Copyright © 2019 BlueShark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
