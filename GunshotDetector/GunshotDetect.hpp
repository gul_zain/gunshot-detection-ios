//
//  GunshotDetect.h
//  GunshotDetector
//
//  Created by admin on 1/12/19.
//  Copyright © 2019 BlueShark. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "XAudio/ModelInterface.h"

NS_ASSUME_NONNULL_BEGIN



@interface GunshotDetect : NSObject
{
    ModelInterface* modelMgr;
}

- (id)init;
- (NSInteger)PredictWithPath : (NSString *)path;
- (NSInteger)PredictWithData : (NSArray*)data;
- (NSInteger)PredictWithSegment : (float*)data : (int)size;
- (NSInteger)PredictWithBuffer : (float*)data : (int)size;
@end

NS_ASSUME_NONNULL_END
