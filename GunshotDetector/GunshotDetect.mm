//
//  GunshotDetect.m
//  GunshotDetector
//
//  Created by admin on 1/12/19.
//  Copyright © 2019 BlueShark. All rights reserved.
//

#import "GunshotDetect.hpp"

@implementation GunshotDetect
{
    NSArray* labels;
}

- (id)init
{
    self = [super init];
    modelMgr = new ModelInterface(2);
    vector<const char*> modelpaths;
    //NSString* gunpath = [[NSBundle mainBundle] pathForResource:@"gunshot_0" ofType:@"model"];
    //NSString* nongunpath = [[NSBundle mainBundle] pathForResource:@"gunshot_1" ofType:@"model" inDirectory:@"GunshotDetector/model"];
    const char *gunpath = [[[NSBundle mainBundle] pathForResource:@"gunshot_0" ofType:@"model"] cStringUsingEncoding:NSASCIIStringEncoding];
    const char *nongunpath = [[[NSBundle mainBundle] pathForResource:@"gunshot_1" ofType:@"model"] cStringUsingEncoding:NSASCIIStringEncoding];
    modelpaths.push_back(gunpath);
    modelpaths.push_back(nongunpath);
    modelMgr->Load(modelpaths);
    labels = [[NSArray alloc] initWithObjects:@"Gunshot", @"NonGunshot", nil];
    return self;
}


- (NSInteger)PredictWithPath : (NSString*)path
{
    int res = -1;
    const char *cString = [path cStringUsingEncoding:NSASCIIStringEncoding];
    res = modelMgr->PredictWithFile(cString);
    return (NSInteger)res;
}

- (NSInteger)PredictWithData:(NSArray *)data
{
    int count = (int)[data count];
    float *wav_data = new float[count];
    int i = 0;
    for(NSNumber *number in data) {
        wav_data[i++] = [number floatValue];
    }
    int res = modelMgr->Predict(wav_data, count);
    return (NSInteger)res;
}

- (NSInteger) PredictWithSegment : (float*)data : (int)size
{
    int res = modelMgr->FindGunshotWithRaw(data, size);
    return (NSInteger)res;
}

- (NSInteger) PredictWithBuffer : (float*)data : (int)size
{
    int res = modelMgr->Predict(data, size);
    return (NSInteger)res;
}


@end
